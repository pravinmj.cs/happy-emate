import sys
from sqlalchemy import create_engine
from os import environ


dbname = environ.get("SQL_DATABASE")
username = environ.get("SQL_USER")
password = environ.get("SQL_PASSWORD")
host = environ.get("SQL_HOST")
port = environ.get("SQL_PORT")


def database_engine():
    try:
        engine = create_engine("postgresql://" + username + ":" + password + "@" + host + ":" + str(port) + "/" + dbname)
    except Exception as e:
        print("An exception has occured: ", e)
        sys.exit()
    return engine


def basic_config():
    return {
        "MAX_RECORDS": 2000,
        "USER_TBNAME": "user",
        "MESSAGE_TBNAME": "message",
        "MESSAGE_RECEPIENT_TBNAME": "message_receipient",
        "MESSAGE_LABEL_TBNAME": "message_label",
        "USER_ACTION_TBNAME": "user_action",
    }
