import base64
import sys
import os
import dateutil
import argparse
import pandas as pd
from datetime import datetime
from auth import service
from bs4 import BeautifulSoup as bS
from email.utils import parsedate_tz, mktime_tz
from config import database_engine, basic_config
from process_sql import insert_bulk_data


config_data = basic_config()
MAX_RECORDS = config_data["MAX_RECORDS"]
USER_TB = config_data["USER_TBNAME"]
MESSAGE_TB = config_data["MESSAGE_TBNAME"]
MESSAGE_RCP_TB = config_data["MESSAGE_RECEPIENT_TBNAME"]
MESSAGE_LB_TB = config_data["MESSAGE_LABEL_TBNAME"]

engine = database_engine()

def get_text_between_symbols(text, open_symbol, close_symbol):
    if open_symbol in text and close_symbol in text:
        return text[text.find(open_symbol)+1:text.find(close_symbol)]
    else:
        return text


def get_available_labels():
    """ Get the available labels in Gmail app for user """
    results = service.users().labels().list(userId='me').execute()
    labels = results.get('labels', [])
    labels_list = [label['name']
                   for label in labels if not label.get('name') is None]
    return labels_list


def get_pagination_bucket(total_records):
    """
        split the total_records to bucket value of 511(max message limit on a single exection)
        Args:
            total_records (int): Get the total records for bucket
        Returns:
            List of integers that sums up to total_records
    """
    pagination_bucket = []
    while total_records != 0:
        if total_records < 511:
            pagination_bucket.append(
                total_records) if total_records > 0 else None
            total_records = 0
        else:
            total_records = total_records - 511
            pagination_bucket.append(511)

    return pagination_bucket


def get_messages(label, max_page_size, page_token=None):
    """
        Retrieve message_ids based on label
        Args:
            lable (str): The label value based on which message will be retrieved
            max_page_size (int): Maximum number of messages retrieved per API call
            page_token (str): Pointer to next page
        Returns:
            List of objects
    """
    if page_token is None:
        result = service.users().messages().list(userId='me',
                                                 labelIds=label,
                                                 maxResults=max_page_size).execute()
    else:
        result = service.users().messages().list(userId='me',
                                                 labelIds=label,
                                                 pageToken=page_token,
                                                 maxResults=max_page_size).execute()
    return result


def get_contents(msg_id):
    """
        Retrieves the full content of a message
        Args:
            msg_id (str): The message's unique Id
        Returns:
            dict of message contents
    """
    data_dict = {}
    data_dict["message_id"] = msg_id
    txt = service.users().messages().get(userId='me',
                                         id=msg['id'],
                                         format="full").execute()

    payload = txt['payload']
    headers = payload['headers']
    data_dict["label_name"] = txt['labelIds']

    for h in headers:
        if h['name'] == 'Subject':
            data_dict["subject"] = h['value']
        if h['name'] == 'From':
            data_dict["sender"] = get_text_between_symbols(
                h['value'], "<", ">")
        if h['name'] == 'To':
            data_dict["recepient"] = get_text_between_symbols(
                h['value'], "<", ">")
        if h['name'] == 'Date':
            timestamp = mktime_tz(parsedate_tz(h['value']))
            data_dict["message_date"] = datetime.fromtimestamp(timestamp)

    # Decrypt the encrypted message body format
    extract_bs = True
    try:
        parts = payload.get('parts')[0]
        data = parts['body']['data']
    except TypeError:  # handle missing formats
        parts = payload.get('body')
        data = parts['data']
    except KeyError:  # For debugging
        data = f"Failed to retrieve for {msg_id}"
        extract_bs = False
        pass

    # Todo: Explore for different lxml format parsing
    # Parse the xlml with bs4
    return data_dict


if __name__ == "__main__":
    labels_list = get_available_labels()
    parser = argparse.ArgumentParser(description='Description of your program')
    parser.add_argument(
        '-l', '--label', help='A Lable name based on which messages will be retrieved', choices=labels_list, required=True, type=str)
    parser.add_argument(
        '-r', '--records', help=f'Maximum records than can be extracted at a given time. Max records that can be extracted is {MAX_RECORDS}', required=True, type=int)
    args = vars(parser.parse_args())

    label = args.get('label')
    total_records = int(args.get('records'))
    if total_records > MAX_RECORDS:
        print(
            f"Max records that can be fetched at given time is {MAX_RECORDS}. Choose a lesser number: ")
        sys.exit()
    pagination = get_pagination_bucket(total_records)
    messages = []
    result = get_messages(label, pagination[0])
    pagination.pop(0)
    if 'messages' in result:
        messages.extend(result['messages'])

    while ('nextPageToken' in result) and len(pagination) > 0:
        page_token = result['nextPageToken']
        result = get_messages(label, pagination[0], page_token)
        messages.extend(result.get('messages'))
        pagination.pop(0)

    message_data = []
    i = len(messages)
    for msg in messages:
        msg_id = msg.get('id')
        msg_content = get_contents(msg_id)
        message_data.append(msg_content)
        i -= 1
        print(f"Processed message id {msg_id}. Pending messages {i}")

    df = pd.DataFrame(message_data)


    label_df = df.set_index(['message_id'])
    label_df = label_df.explode('label_name').reset_index()

    label_df = label_df[['message_id', 'label_name']]

    message_df = df[['message_id', 'message_date', 'sender', 'subject']]
    recepient_df = df[['message_id', 'recepient']]

    user_df = pd.DataFrame()
    sender_list = df['sender'].to_list()
    receiver_list = df['recepient'].to_list()

    email_list = set(sender_list + receiver_list)
    user_df['email'] = pd.DataFrame(email_list)


    status_user_data, usr_total = insert_bulk_data(user_df, USER_TB)
    status_label_data, lb_total = insert_bulk_data(label_df, MESSAGE_LB_TB)
    status_message_data, msg_total = insert_bulk_data(message_df, MESSAGE_TB)
    status_recepient_data, rcp_total = insert_bulk_data(recepient_df, MESSAGE_RCP_TB)

    print(f"User Data inserted: {usr_total}")
    print(f"Total Message rows inserted: {msg_total}")
    print(f"Total Label rows inserted: {lb_total}")
    print(f"Total Recepient rows inserted: {rcp_total}")