import os
import sys
import pickle
import base64
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from bs4 import BeautifulSoup

SCOPES = ["https://mail.google.com/"]
access_email = "" # Your email

try:
    def authenticate_gmail():
        """
        Authenticate on behalf of access_email. Creates and manages access and refresh tokens for access_email.
        Creates token and pickle file automatically intially at first time by logging access_email based on secrets.json oauth file
        """
        access_creds = None
        if os.path.exists("access_token.pickle"):
            with open("access_token.pickle", "rb") as token:
                access_creds = pickle.load(token)

        if not access_creds or not access_creds.valid:
            if access_creds and access_creds.refresh_token and access_creds.expired:
                access_creds.refresh(Request())
            else:
                try:
                    flow = InstalledAppFlow.from_client_secrets_file("secrets.json", SCOPES)
                    access_creds = flow.run_local_server(port=0)
                except FileNotFoundError:
                    print("secrets.json file missing. please maintain a oauth client credentials file in project root dir")
            # save the credentials for the next run
            with open("access_token.pickle", "wb") as token:
                pickle.dump(access_creds, token)
        return build("gmail", "v1", credentials=access_creds)


    # get the Gmail API service

    service = authenticate_gmail()
except Exception as e:
    print("Exception has occured: ", e)
    sys.exit(0)