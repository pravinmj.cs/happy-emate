from config import database_engine
import pandas as pd

engine = database_engine()


def process_query(query):
    try:
        df = pd.read_sql_query(query, engine)
        status = True
    except Exception as e:
        print(e, "*******")
        df = pd.DataFrame([])
        status = False
    return df, status
