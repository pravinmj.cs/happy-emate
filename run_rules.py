import os
import sys
import argparse
import json
import pandas as pd
from process_rule_sql import process_query
from process_sql import insert_bulk_data


def get_available_rules():
    files = [f for f in os.listdir(os.chdir("./Rules")) if os.path.isfile(f)]
    return files


def available_config():
    """ import from available columns of etl df. Configure the predicates, conditions and actions to be used"""

    # Addional operators can be added for predicates
    fields = [
        {
            "name": "From",
            "alt": "sender",
            "table": "message",
            "type": str,
            "predicates": [
                {"value": "contains", "operator": "~"},
                {"value": "not contains", "operator": "!~"},
                {"value": "equals", "operator": "="},
            ],
        },
        {
            "name": "To",
            "alt": "recepient",
            "table": "message_receipient",
            "type": str,
            "predicates": [
                {"value": "contains", "operator": "~"},
                {"value": "not contains", "operator": "!~"},
                {"value": "equals", "operator": "="},
            ],
        },
        {
            "name": "Subject",
            "alt": "subject",
            "table": "message",
            "type": str,
            "predicates": [
                {"value": "contains", "operator": "~"},
                {"value": "not contains", "operator": "!~"},
                {"value": "equals", "operator": "="},
            ],
        },
        {
            "name": "Date Received",
            "alt": "message_date",
            "table": "message_receipient",
            "type": int,
            "predicates": [{"value": "less than days", "type": ["months", "days"], "operator": "<"}],
        },
    ]
    # Any custome labels can be added and modified in future
    actions = [
        {"action": "mark as read", "drop_value": ["UNREAD"], "alias_label": "READ"},
        {"action": "mark as unread", "drop_value": ["READ"], "alias_label": "UNREAD"},
        {"action": "mark as starred", "drop_value": ["UNSTARRED"], "alias_label": "STARRED"},
        {"action": "mark as unstarred", "drop_value": ["STARRED"], "alias_label": "UNSTARRED"},
        {"action": "mark as important", "drop_value": ["NOT IMPORTANT"], "alias_label": "IMPORTANT"},
        {"action": "mark as not important", "drop_value": ["IMPORTANT"], "alias_label": "NOT IMPORTANT"},
        {"action": "move message to inbox", "drop_value": ["TRASH", "SPAM"], "alias_label": "INBOX"},
        {"action": "move message to spam", "drop_value": ["INBOX", "TRASH"], "alias_label": "SPAM"},
        {"action": "move message to trash", "drop_value": ["SPAM", "INBOX"], "alias_label": "TRASH"},
    ]
    conditions = {"All": "and", "Any": "or"}

    return fields, actions, conditions


def validate_rule(rule_object, object_type):
    fields, actions, conditions = available_config()
    status = False
    try:
        if object_type == "Field":
            for field in fields:
                if field["name"] == rule_object["field_name"]:
                    for v in field["predicates"]:
                        if rule_object["predicate"] == v["value"]:
                            rule_object["operator"] = v["operator"]
                            rule_object["type"] = str
                            rule_object["alias"] = field["alt"]
                            rule_object["table"] = field["table"]
                            # rule_object["aliasasdasd"] = v["alt"]
                            status = True

                        if rule_object["field_name"] == "Date Received":
                            rule_object["type"] = int
                            if rule_object["date_type"] not in v["type"]:
                                status = False

                            if rule_object.get("value") is None or rule_object.get("value") == "":
                                status = False

        elif object_type == "Action":
            for action in actions:
                if rule_object["action"] == action["action"]:
                    rule_object["alias_label"] = action["alias_label"]
                    rule_object["drop_value"] = action["drop_value"]
                    status = True
                    break
    except KeyError:
        pass
    return status, rule_object


def rule_query_builder(rule_list, condition):
    """ Build a query based on the available_config func """
    base_query = "select * from message inner join message_receipient  on message_receipient.message_id = message.message_id where "

    i = 1
    for rule in rule_list:  # enum

        if i < len(rule_list):
            if isinstance(rule["type"], int) is False:
                base_query = (
                    base_query + f"{rule['table']}.{rule['alias']} {rule['operator']} '{rule['value']}' {condition} "
                )
            else:  #Todo: Pending query builder for date logic. Slight modiufcation to base query with extra date fields
                base_query = ""

        else:
            base_query = base_query + f"{rule['table']}.{rule['alias']} {rule['operator']} '{rule['value']}'"
        i = i + 1

    
    return base_query


def rule_action_query_builder(action, message_ids):  # Todo: Combine to single query?
    """ Drop and update labels based on action elements in basic_config"""
    if len(message_ids) == 1:
        message_id_list = f"'{message_ids[0]}'"
    else:
        message_id_list = ",".join([f"'{str(v)}'" for v in message_ids])

    action_value = action["drop_value"]
    if len(action_value) == 1:
        action_labels = f"'{action_value[0]}'"
    else:
        action_labels = ",".join([f"'{str(v)}'" for v in action_value])

    base_query = (
        f"delete from message_label where message_id in ({message_id_list}) and label_name in ({action_labels})"
    )

    if len(action["alias_label"]) > 0:
        action_label = action["alias_label"]
    insert_df = pd.DataFrame([])
    insert_df["message_id"] = message_ids
    insert_df["label_name"] = action_label

    return base_query, insert_df


if __name__ == "__main__":
    rule_list = get_available_rules()
    rule_fields, rule_actions, rule_conditions = available_config()
    parser = argparse.ArgumentParser(description="Perform action based on rules")
    parser.add_argument(
        "-r",
        "--rule",
        help="Rule file to run from Rules directory",
        choices=rule_list,
        required=True,
    )
    args = vars(parser.parse_args())

    rule_file = args.get("rule")
    script_dir = os.path.dirname(__file__)
    file_path = os.getcwd()
    with open(file_path + "/" + rule_file, "r") as f:
        rules = json.loads(f.read())

    if rules["rule_condition"] not in rule_conditions:
        print(f"Invalid Rule conditions. Rule conditons allowed are {','.join(rule_conditions)}")
        sys.exit()

    field_names = []
    table_name = "message"
    operator = "~"
    field_value = ""

    rule_values = rules.get("rule_values")
    rule_description = rules.get("rule_description")
    rule_actions = rules.get("rule_actions")
    rule_condition = rules.get("rule_condition")

    formatted_rule_values = []
    formatted_action_values = []

    field_validation_failed = 0

    i = 0
    for value in rule_values:
        status, rule_object = validate_rule(value, "Field")
        if status is False:
            field_validation_failed += 1
        formatted_rule_values.append(rule_object)

    if field_validation_failed > 0:  # Todo: Format to give precise exception message
        print("\n One or more rules contains invalid input. Please correct the rule file and try again \n")
        sys.exit(0)

    action_validation_failed = 0
    for value in rule_actions:
        status, action_object = validate_rule(value, "Action")
        if status is False:
            action_validation_failed += 1
        formatted_action_values.append(action_object)
        i = i + 1

    if action_validation_failed > 0:  # Todo: Format to give precise exception message
        print("\n One or more actions contains invalid input. Please correct the rule file and try again \n")
        sys.exit(0)

    condition_op = rule_conditions.get(rule_condition)
    if condition_op is None:
        print("\n One or more conditions contains invalid input. Please correct the rule file and try again \n")
        sys.exit(0)
    query_string = rule_query_builder(formatted_rule_values, condition_op)

    df, status = process_query(query_string)

    records_available = len(df)
    if records_available > 0:
        print(f"Total matching records available: {records_available}")

    try:
        message_ids = df["message_id"].values.tolist()
    except KeyError:
        message_ids = []


    new_msg_id_list = []  # To remove duplicates
    for msg in message_ids:
        if not msg[0] in new_msg_id_list:
            new_msg_id_list.append(msg[0])


    for action in formatted_action_values:
        if  len(new_msg_id_list) >= 1:
            query, insert_df = rule_action_query_builder(action, new_msg_id_list)
            df, status = process_query(query)
            action_status, total = insert_bulk_data(insert_df, "message_label")
 
        # print(status, action_status)

    # print(new_msg_id_list)
    # print(insert_df)