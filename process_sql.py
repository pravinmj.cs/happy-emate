from config import database_engine

engine = database_engine()


def insert_bulk_data(df, tablename):
    try:
        print(df, "************")
        df.to_sql(tablename, engine, if_exists="append", index=False)
        total = len(df)
        status = True
    except Exception as e:
        print("&&&&&&&&&", e)
        status = False
        total = 0

    return status, total
