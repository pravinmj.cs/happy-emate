# Happy-Emate

Automate email management with happy-emate

## Related py files
- [etl.py](https://gitlab.com/pravinmj.cs/happy-emate/-/blob/main/etl.py)
- [run_rules.py](https://gitlab.com/pravinmj.cs/happy-emate/-/blob/main/run_rules.py)
## Getting started

Project milestone at [Milestone 01](https://gitlab.com/pravinmj.cs/happy-emate/-/milestones/1)

- Install requirements.txt
- Configure gmail Oauth client and download credentials as secrets.json. Place this in the root directory
- Enter your email in access_email of auth.py (To do - ENV var)
- Configure postgres database and import relevant environment variables mentioned in config.py
- Run etl.py - Confirms the web client login for the first time.

- The job etl runs with command line flags of label name('INBOX', 'IMPORTANT') to retrieve email specific to the label and number of records that can be extracted (Max is set as 3000 which can be changed)
- The job will be upload to database based on below schema

### Schema

![alt text](https://gitlab.com/pravinmj.cs/happy-emate/uploads/6d98584d7249caae76497faa1fa134db/HappyEMate.png)

- Now define rule files in Rules directory
- Any number of rule file can be maintained and called with -r rule_filename while running run_rules.py
- Rule runs based on matching configurations in basic_config() of run_rules.py file. Please refer and modify it. Due to time constraints, I can customize to a greater level. But i thought I can set a base on which it can be updated. 
- The user_action table was created with an intention of saving analytics so another script can be used to generate a PDF/excel report based on it.

